#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fuse.h>
#include "grow.h"
#include "parser.h"
#include "cache.h"
#include "canonize.h"
#include "urlencode.h"

/* 
 * url canonization process
 */

/* terrible code, FIXME
 *
 * few ideas for making ordinary web pages more-or-less browsable
 *
 * 1) if occurs dir1/dir2/page as url in current pake, mark dir1 as directory
 * 2) if occurs /dir1/dir2/dir3/page as url, and we are in /dir1/dir2/dir3 add page 
 * 3) what about symlinks to different part of the site?
 */


char *prefixes[] = { "http://", "ftp://", "https://", "mailto:", "/", NULL };


static inline int bad_prefix(char *s)
{
	int i;
	for (i=0; prefixes[i]; i++) {
		if (!strncasecmp(prefixes[i], s, strlen(prefixes[i])))
			return 1;
	}
	return 0;
}


void canonize(void *ctx, char *par, time_t time, size_t size)
{
	struct canon *c = ctx;
	struct stat stb;
	struct grow g;

	memset(&stb, 0, sizeof(stb));

	if (bad_prefix(par))
		return;

	if (par[strlen(par)-1] == '/') {
		stb.st_mode = S_IFDIR | 0555;
		par[strlen(par)-1] = 0;
	} else {
		stb.st_mode = S_IFREG | 0444;
	}

	if (strchr(par, '/') || strchr(par, '#') || par[0] == '?')
		return;
	if (par[0] == 0)
		return;

	grow_init(&g);
	path_decode(&g, par);
	grow_put(&g, 0);
	c->filler(c->h, g.s, &stb);
	grow_free(&g);
}
