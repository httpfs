#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <curl/curl.h>
#include <error.h>
#include <errno.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include "debug.h"
#include "grow.h"
#include "fetch.h"

/*
 * TODO:
 * 	fix connect-timeouts
 */


static int cb_write(char *data, size_t size, size_t nmemb, struct grow *gr) 
{
/* 	printf("zapisuju %d do %d, %d\n", (int) (size * nmemb), gr->ptr, gr->size); */
	return grow_append(gr, data, size*nmemb);
}

/* Date: Tue, 23 Oct 2007 23:42:30 GMT */
/* Last-Modified: Mon, 22 Oct 2007 03:25:53 GMT */

#define DATE_HDR "Date: "
#define LASM_HDR "Last-Modified: "
#define CONL_HDR "Content-length: "
#define SCC(s,q) !strncasecmp(s,q,strlen(q))

static int cb_stat(char *data, size_t size, size_t nmemb, struct stat *st)
{
	size_t sz = size * nmemb;

	/* XXX buffer not ended with 0 */
	if (SCC(data, DATE_HDR)) {
		if (!st->st_mtime) 
			st->st_mtime = curl_getdate(data+strlen(DATE_HDR), NULL);
	} else if (SCC(data, LASM_HDR)) {
		st->st_mtime = curl_getdate(data+strlen(LASM_HDR), NULL);
	} else if (SCC(data, CONL_HDR)) {
		st->st_size = (long long) atol(data+strlen(CONL_HDR));
	}

	return sz;
}

static inline int unixerr(CURLcode ret, long errcode, int data)
{
	switch(ret) {
		case CURLE_OK:
			break;
		case CURLE_COULDNT_RESOLVE_HOST:
			return -ENOENT;
		case CURLE_OPERATION_TIMEDOUT:
			return -ETIMEDOUT;
/* 		case CURLE_RANGE_ERROR: */
/* 			return -ESPIPE; */
		default:
			if (errcode == 416) 
				return -ESPIPE;
			return -EIO;
	}

	switch(errcode) {
		case 404: return -ENOENT;
		case 403: return -EPERM;
		case 301: return -EMLINK;
		case 200:
		case 206: return data;
		default: return -EBUSY;
	}
}


/* NASTY HACK */
void wait(CURLM *handle)
{
	fd_set r, w, e;
	int maxfd;
	struct timeval tv;

	tv.tv_sec = 0;
	tv.tv_usec = 10000;

	dbg("fetch: waiting\n");
	curl_multi_fdset(handle, &r, &w, &e, &maxfd);
	select(maxfd, &r, &w, &e, &tv);
}

#define CH(s) { CURLcode _code; _code = s; if (_code != CURLE_OK) { fprintf(stderr, "Error: %s\n%s\n", #s, errbuf); return -EIO; } }

int request(CURL *conn, char *url, int head, struct stat *st, struct grow *data, size_t size, off_t offset)
{
	char range[32];
	char errbuf[CURL_ERROR_SIZE];
	CURLcode ret;
	long errcode;

	dbg("fetch url:\"%s\" head:%d st:%p data:%p size:%lu off:%ld\n", url, head, st, data, size, offset);

/* 	CH(curl_easy_setopt(conn, CURLOPT_VERBOSE, 1)); */
	CH(curl_easy_setopt(conn, CURLOPT_ERRORBUFFER, errbuf));
	CH(curl_easy_setopt(conn, CURLOPT_URL, url));
	CH(curl_easy_setopt(conn, CURLOPT_WRITEFUNCTION, data ? cb_write : NULL));
	CH(curl_easy_setopt(conn, CURLOPT_WRITEDATA, data ? data : NULL));
	CH(curl_easy_setopt(conn, CURLOPT_HEADERFUNCTION, st ? cb_stat : NULL));
	CH(curl_easy_setopt(conn, CURLOPT_HEADERDATA, st ? st : NULL));
	CH(curl_easy_setopt(conn, CURLOPT_NOBODY, head));
/* 	CH(curl_easy_setopt(conn, CURLOPT_TIMEOUT, 30)); */
	if (head) {
		CH(curl_easy_setopt(conn, CURLOPT_CUSTOMREQUEST, "HEAD"));
	} else {
		CH(curl_easy_setopt(conn, CURLOPT_CUSTOMREQUEST, NULL));
		CH(curl_easy_setopt(conn, CURLOPT_HTTPGET, 1));
	}
	
	if (size > 0) {
		/* FIXME: may overflow when downloading larger files (eg iso images) */
		snprintf(range, 32, "%ld-%ld", (long) offset, (long) (offset+size-1));
		dbg("range: \"%s\"\n", range);
		CH(curl_easy_setopt(conn, CURLOPT_RANGE, range));
	} else {
		CH(curl_easy_setopt(conn, CURLOPT_RANGE, NULL));
	}

	ret = curl_easy_perform(conn);
	curl_easy_getinfo(conn, CURLINFO_RESPONSE_CODE, &errcode);

	dbg("fetch done, ret:%d code: %ld\n", ret, errcode);
	return unixerr(ret, errcode, data?data->ptr:0);
}

/* nebylo by treba storovat tady i CURL */
int request_open_async(struct handle *h, off_t offset, size_t size)
{
	char errbuf[CURL_ERROR_SIZE];
	CURLM *mult;
	CURL *conn;

	dbg("setupuju async\n");

	mult = curl_multi_init();
	if (!mult)
		return -EIO;
	
	conn = curl_easy_init();
	if (!conn) {
		curl_multi_cleanup(mult);
		return -EIO; 
	}

	CH(curl_easy_setopt(conn, CURLOPT_ERRORBUFFER, errbuf));
	CH(curl_easy_setopt(conn, CURLOPT_VERBOSE, 1));
	CH(curl_easy_setopt(conn, CURLOPT_URL, h->url.s));
	CH(curl_easy_setopt(conn, CURLOPT_WRITEFUNCTION, cb_write));
	CH(curl_easy_setopt(conn, CURLOPT_WRITEDATA, &h->lover));
/* 	CH(curl_easy_setopt(conn, CURLOPT_TIMEOUT, 30)); */
	if (offset > 0) {
		snprintf(h->range, 32, "%ld-%ld", (long) offset, (long) (offset+size-1));
		dbg("range: \"%s\"\n", h->range);
		CH(curl_easy_setopt(conn, CURLOPT_RANGE, h->range));
	}

	CH(curl_multi_add_handle(mult, conn));

	h->conn = conn;
	h->mult = mult;
	return 0;
}

void request_close_async(struct handle *h)
{
	dbg("async; cleanup\n");
	if (!request_done(h)) {
		curl_multi_remove_handle(h->mult, h->conn);
		curl_easy_cleanup(h->conn);
		curl_multi_cleanup(h->mult);
	}
	h->conn = NULL;
	h->mult = NULL;
}

int request_perf_async(struct handle *h, size_t todo, int *done)
{
	int left, ret = -1;
	long code;
	CURLMsg *msg;

	dbg("async: todo: %d, ctu %p %p, mam precist %d\n", todo, h->conn, h->mult, h->lover.size);
	while (h->lover.ptr < todo) {
		if (ret == 0) 
			wait(h->mult);

		ret = curl_multi_perform(h->mult, &left);

		if (ret > 0) {
			dbg("--- MULTI PERFORM ERROR %d ---\n", ret);
			exit(1);
		}

		while((msg = curl_multi_info_read(h->mult, &left))) {
			curl_easy_getinfo(msg->easy_handle, CURLINFO_RESPONSE_CODE, &code);
			if (msg->msg == CURLMSG_DONE) {
				int ret = unixerr(msg->data.result, code, h->lover.ptr);
				dbg("async: konec %d %d\n", msg->data.result, code);
				*done = 1;
				return ret;
			}
		}
	}

	dbg("async: vracim %d\n", h->lover.ptr);
	return h->lover.ptr;
}
