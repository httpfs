#include <curl/curl.h>
#include <pthread.h>

typedef enum {
	HAND_HARD=1,
	HAND_RANDOM=2,
} hand_type_t;

/* TODO: lockovani */
struct handle {
	hand_type_t type;
	struct grow url;
	struct grow data;
	struct grow lover; 

	off_t in_order, out_order;
	off_t offset;
	size_t total;
	/* TODO: zjistit proc curl potrbeuje funkcni ptr na range az do konce provedeni requestu */
	char range[32];

	CURLM *mult;
	CURL *conn;
	pthread_mutex_t async_lock;
};


int request(CURL *conn, char *url, int head, struct stat *st, struct grow *data, size_t size, off_t offset);
int request_open_async(struct handle *h, off_t offset, size_t size);
void request_close_async(struct handle *h);
int request_perf_async(struct handle *h, size_t todo, int *done);
#define request_done(h) (!(h)->mult)
