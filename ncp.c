/*
 * ncp - Neat CoPy
 *
 * Very simple 'cp' implementation with progressbar and resume.
 * Usefull with sshfs/httpfs and other network filesystems.
 * 
 * TODO
 * 	use another thread for progressbar instead of sigalarm -> blocked read
 */

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include "debug.h"
#include "grow.h"

#define DEL_LINE "\e[2K"
#define INV_ON "\e[7m"
#define INV_OFF "\e[27m"
#define BS 4096

#define BAR_BITS 5
#define BAR_WIDTH (1<<BAR_BITS)
#define AVG_SECS 15
#define CUR_SECS 2
#define PROG_WIDTH 30ULL

#define step(b,n) ((b)->xfer[(b->step+(n))&((1<<BAR_BITS)-1)])
#define MAX(x,y) ((x)>(y)?(x):(y))
#define MIN(x,y) ((x)<(y)?(x):(y))

#define DEFAULT_DISPLAY (D_PROG | D_SPEED | D_SIZE | D_NAME)

typedef enum {
	D_BAND = 1,
	D_PROG = 2,
	D_NAME = 4,
	D_ETA  = 8,
	D_SPEED = 16,
	D_SIZE = 32,
} display_t;

struct bar {
	display_t display;
	int xfer[BAR_WIDTH];
	time_t start, last;
	size_t resumed, xfered, total; 
	int step, peak, accum;
	int avg_speed, cur_speed;
	const char *filename;
};

struct config {
	int force_dir;
	int recursive;
	int resume;
};


char buffer[BS];
struct bar progress;
display_t display_opt = DEFAULT_DISPLAY;

static inline void pexit(char *s)
{
	perror(s);
	exit(1);
}

static inline void sexit(const char *fn, const char *s)
{
	fprintf(stderr, "%s: %s\n", fn, s);
	exit(1);
}

static inline void serror(const char *fn, const char *s)
{
	fprintf(stderr, "%s: %s\n", fn, s);
}


static inline const char *basename(const char *s)
{
	char *p;

	p = strrchr(s, '/');
	if (!p)
		return s;
	return (p+1);
}

static inline void remove_last(struct grow *g, int c)
{
	int len = g->ptr;
	while (len && g->s[len-1] == c) {
		g->s[len-1] = 0;
		len--;
	}
	g->ptr = len;
}

/* we completely ignore the fact that alarms can be delayed or that race 
 * condition could occur */

static char *show_unit(double val)
{
	char *c = "\0KMGT";
	static char buffer[128];

	while (val >= 1000 && *(c+1)) {
		val /= (double) 1024;
		c++;
	}
	snprintf(buffer, 128, "%3.1lf %c", val, *c);
	return buffer;
}

static char *show_time(int sec)
{
	static char buffer[128];

	snprintf(buffer, 128, "%3d:%02d", sec/60, sec%60);
	return buffer;
}

static void show_band(struct bar *b)
{
	int i;
	double peak;

	if (!b->peak) 
		peak = 100;
	else
		peak = (double) (b->peak + 1);

	for (i=1; i<=BAR_WIDTH; i++) {
		int idx = 5.0 * (double) step(b, -i) / peak;
		putchar(" _.oO"[idx]);
/* 		putchar(" _.-~"[idx]); */
	}
	
}

static void show_prog(struct bar *b)
{
	char buf[PROG_WIDTH], fmt[8];
	int rev, perc, i;
	
	rev = (int)((b->xfered + b->resumed) * PROG_WIDTH / b->total);
	perc = (int)((b->xfered + b->resumed) * 100ULL / b->total);
	snprintf(fmt, 8, "%3d%%", perc);

	memset(buf, ' ', PROG_WIDTH);
	memcpy(buf+PROG_WIDTH/2-2, fmt, 4);

	printf("[" INV_ON);
	for (i=0; i<PROG_WIDTH; i++) {
		if (i == rev) 
			printf(INV_OFF);
		putchar(buf[i]);
	}
	printf(INV_OFF "]");
}



void update_stats(struct bar *b, size_t accum)
{
	int i, peak;

	/* we transfered some more */
	b->xfered += accum;

	/* set speed in sec */
	step(b,0) = accum;

	/* update speed stats */
	b->cur_speed += accum - step(b,-CUR_SECS);
	b->avg_speed += accum - step(b,-AVG_SECS);

	/* calculate peak in last BAR_WIDTH secs */
	peak = b->xfer[0];
	for (i=1; i<BAR_WIDTH; i++) 
		peak = MAX(b->xfer[i], peak);
	b->peak = peak;

	/* step forward */
	b->step++;
}

void show_stats(struct bar *b)
{
	double avg, cur;
	int eta;

	avg = (double) b->avg_speed / (double) MIN(AVG_SECS, b->step);
	cur = (double) b->cur_speed / (double) MIN(CUR_SECS, b->step);

	printf("\r");

	if (b->display & D_BAND) 
		show_band(b);
	if (b->display & D_PROG) 
		show_prog(b);
	if (b->display & D_ETA) {
		if (avg > 0) {
			eta = (double) (b->total - b->resumed - b->xfered) / avg;
			printf(" ETA %s", show_time(eta));
		} else {
			printf(" ETA  --:--");
		}
	}
	if (b->display & D_SPEED) {
		printf(", %sB/s", show_unit(cur));
	}
	if (b->display & D_SIZE) {
		printf(", %sB of", show_unit(b->xfered+b->resumed));
		printf(" %sB", show_unit(b->total));
	}
	if (b->display & D_NAME) {
		printf(", %s", b->filename);
	}

	printf("\e[K");
	fflush(stdout);
}

static void update(int sig)
{
	struct bar *b = &progress;
	time_t now = time(NULL), steps=0;
	size_t accum_one;

	alarm(1);

	steps = MAX(1, now - b->last);
	accum_one = (b->accum + steps - 1) / steps;
	while(steps--)
		update_stats(b, accum_one);
	b->last = now;
	b->accum = 0;
	show_stats(b);
}

static inline void init_bar(struct bar *b, const char *filename, size_t total , size_t resumed)
{
	memset(b, 0, sizeof(*b));
	b->filename = filename;
	b->display = display_opt;
	b->start = time(NULL);
	b->resumed = resumed;
	b->total = total;
	b->last = time(NULL);
	signal(SIGALRM, update);
	alarm(1);
}

static inline void fini_bar(struct bar *b)
{
	signal(SIGALRM, SIG_IGN);
	update(0);
	printf("\r%s -- %s\e[K\n", show_time(time(NULL)-b->start), b->filename);
}

static inline void update_bar(struct bar *b, int data)
{
	b->accum += data;
}

	
static int do_copy(char *src, int sfd, int dfd, off_t src_size, off_t dst_size)
{
	off_t src_pos=0, dst_pos=0;
	int ret, wrt;


	dbg("total size %llu, resume from %lld\n", src_size, dst_size);

	if (dst_size >= src_size) {
		dbg("Already done.\n");
		return 0;
	}

	if (dst_size > 0) {
		src_pos = lseek(sfd, dst_size, SEEK_SET);
		if (src_pos < 0 || src_pos != dst_size) {
			perror("Cannot seek in input file");
			return -1;
		}

		dst_pos = lseek(dfd, dst_size, SEEK_SET);
		if (dst_pos < 0 || dst_pos != dst_size) {
			perror("Cannot seek in output file");
			return -1;
		}

		dbg("Resuming.\n");
	}

	init_bar(&progress, basename(src), src_size, dst_size);
	while (1) {
againread:
		ret = read(sfd, buffer, BS);
		if (ret < 0) {
			if (errno == EAGAIN) {
				dbg("Interrupted.");
				goto againread;
			}
			perror("Reading input file");
			return -1;
		}
		if (ret == 0) {
			if (dst_pos == src_size || !src_size) {
				fini_bar(&progress);
				return 0;
			}
			fini_bar(&progress);
			dbg("Unexpected end of source.\n");
			return 1;
		}

againwrite:
		wrt = write(dfd, buffer, ret);
		if (wrt < 0) {
			if (errno == EAGAIN) {
				dbg("Interrupted.");
				goto againwrite;
			}
			perror("Writing output file");
			return -1;
		}
		if (wrt < ret) {
			perror("Too few bytes written");
			return -1;
		}

		dst_pos += wrt;
		src_pos += wrt;

		update_bar(&progress, wrt);
	}
}

static int mkdirp(const char *dir, int mode)
{
	char buffer[PATH_MAX];
	const char *p, *q;
	int ret, len;

	q = p = dir;

	while (p) {
		for (; *p == '/'; p++);
		if (!*p)
			break;

		q = strchr(p, '/');
		if (!q) 
			len = strlen(dir);
		else
			len = q-dir;

		len = MIN(len, PATH_MAX-1);
		memcpy(buffer, dir, len);
		buffer[len] = 0;

		ret = mkdir(buffer, mode);
		if (ret && errno != EEXIST)
			return -1;

		p = q;
	}
	return 0;
}


static int copy(struct grow *src, struct grow *dst, struct config *conf);

static int copydir(struct grow *src, struct grow *dst, struct config *conf)
{
	DIR *d;
	struct dirent *ent;
	size_t optr, nptr;

	d = opendir(grow_cstr(src));
	if (!d) {
		perror(grow_cstr(src));
		return 0;
	}
	while ((ent = readdir(d))) {
		if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
			continue;

		optr = grow_get_ptr(src);
		nptr = grow_get_ptr(dst);

		grow_put(src, '/');
		grow_puts(src, ent->d_name);
		copy(src, dst, conf);

		grow_set_ptr(src, optr);
		grow_set_ptr(dst, nptr);
	}
	closedir(d);
	return 0;
}


static int copy(struct grow *srcg, struct grow *dstg, struct config *conf)
{
	struct stat sb, db;
	int sfd, dfd, found, ret=-1;
	char *src, *dst;

	src = grow_cstr(srcg);
	dst = grow_cstr(dstg);

	if (stat(src, &sb) < 0) {
		perror(src);
		return -1;
	}

	if (sb.st_mode & S_IFREG && sb.st_size <= 0) {
		/* httpfs trick */
		sleep(1);
		stat(src, &sb);
	}

	if (sb.st_mode & S_IFDIR) {
		int ret;
		size_t optr;

		if (!conf->recursive) {
			serror(src, "Ommiting directory");
			return -1;
		}

		conf->force_dir++;
		optr = grow_get_ptr(dstg);
		grow_put(dstg, '/');
		grow_puts(dstg, basename(src));

		if (mkdirp(grow_cstr(dstg), 0777)) 
			return -1;

		ret = copydir(srcg, dstg, conf);

		grow_set_ptr(dstg, optr);
		conf->force_dir--;
		return ret;
	}

	if (!(sb.st_mode & S_IFREG)) {
		serror(src, "Not a regular file");
		return -1;
	}

	sfd = open(src, O_RDONLY);
	if (sfd < 0) {
		perror(src);
		return -1;
	}


	found = !stat(dst, &db);
	if (found) {
		if (db.st_mode & S_IFDIR) {
			grow_put(dstg, '/');
			grow_puts(dstg, basename(src));
			dst = grow_cstr(dstg);
			found = !stat(dst, &db);
		} else if (conf->force_dir) {
			serror(dst, "Destination is not a directory");
			goto sfd_close;
		}

		if (found) {
			if (!(db.st_mode & S_IFREG)) {
				serror(dst, "Destination not a regular file");
				goto sfd_close;
			}

			if (!conf->resume) {
				serror(src, "File exists");
				goto sfd_close;
			}

			dfd = open(dst, O_WRONLY);
			if (dfd < 0) {
				perror(dst);
				goto sfd_close;
			}

			ret = do_copy(src, sfd, dfd, sb.st_size, db.st_size);
			goto dfd_close;
		}
	} else if (conf->force_dir) {
		serror(dst, "Destination is not a directory");
		goto sfd_close;
	}

	dfd = open(dst, O_CREAT|O_WRONLY, 0666);
	if (dfd < 0) {
		perror(dst);
		goto sfd_close;
	}

	ret = do_copy(src, sfd, dfd, sb.st_size, 0);
dfd_close:
	close(dfd);
sfd_close:
	close(sfd);
	return ret;
}

static void usage(char *me)
{
	printf("usage: %s [-b] [-c] [-e] SOURCE(s) DEST\n", me);
	printf("\t-b show bandwidth bar\n\t-c resume downloads\n\t-e show ETA\n");
	exit(1);
}

char source[PATH_MAX], destination[PATH_MAX];

/* TODO: overwrite, recursive */
int main(int argc, char *argv[])
{
	int nfiles=0, dstidx=0;
	struct grow src, dst;
	struct config conf;
	int i;

	memset(&conf, 0, sizeof(conf));
	for (i=1; i<argc; i++) {
		if (argv[i][0] == '-') {
			switch(argv[i][1]) {
				case 'r':
					conf.recursive = 1;
					break;
				case '0':
					display_opt = D_BAND  | D_SPEED | D_SIZE;
					break;
				case 'b':
					display_opt ^= D_BAND | D_PROG;
					break;
				case 'e':
					display_opt ^= D_ETA;
					break;
				case 'n':
					display_opt ^= D_NAME;
					break;
				case 'c':
					conf.resume = 1;
					break;
				case 'h':
					usage(argv[0]);
					break;
				default:
					fprintf(stderr, "Unknown opt %c\n", argv[i][1]);
					break;
			}
			continue;
		}
		nfiles++;
		dstidx=i; 
	}

	if (nfiles < 2) 
		usage(argv[0]);
	if (nfiles > 2)
		conf.force_dir=1;

	for (i=1; i<dstidx; i++) {
		if (argv[i][0] == '-')
			continue;

		grow_init_static(&src, source, PATH_MAX);
		grow_init_static(&dst, destination, PATH_MAX);
		grow_puts(&src, argv[i]);
		grow_puts(&dst, argv[dstidx]);
		remove_last(&src, '/');
		remove_last(&dst, '/');

		copy(&src, &dst, &conf);
	}
	return 0;
}


