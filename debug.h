#ifdef EBUG
#define dbg(p...) fprintf(stderr, p)
#else
#define dbg(...)
#endif
