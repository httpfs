#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "debug.h"
#include "grow.h"
#include "parser.h"
#include "urlencode.h"


struct tag {
	char *name;
	char *param;
} grab_tags[] = {
	{ "a", "href" },
	{ "applet", "code" },
	{ "area", "href" },
	{ "embed", "code" },
	{ "frame", "src" },
	{ "iframe", "src" },
	{ "img", "src" },
	{ "img", "href" },
	{ "link", "href" },
	{ "script", "src" },
	{ NULL, NULL }
};

static inline int good_tag(char *tn, char *pn)
{
	int i;
	struct tag *t;

	for (i=0; t = &grab_tags[i], t->name; i++) {
		if (!strcmp(t->name, tn) && !strcmp(t->param, pn))
			return 1;
	}
	return 0;
}


#define otag(tn,pn,pv,cb,ctx) { if (good_tag(grow_cstr(tn), grow_cstr(pn))) cb(ctx, grow_cstr(pv), 0, 0); grow_reset(pv); grow_reset(pn); }
void generic_parse(char *input, parser_cb_t cb, void *ctx)
{
	struct grow tn, pn, pv;
	int c, state = 0;

	grow_init(&tn);
	grow_init(&pn);
	grow_init(&pv);

	while ((c=*input++)) {
		switch(state) {
		case 0:
			if (c == '<')
				state = 1;
			break;
		case 1:
			if (c == '/' || isalpha(c)) {
				grow_reset(&tn);
				grow_put(&tn,tolower(c));
				state = 2;
			}
			break;
		case 2:
			if (!isalpha(c)) {
				if (c == '>')
					state = 0;
				else
					state = 3;
			} else {
				grow_put(&tn,tolower(c));
			}
			break;
		case 3:
			if (c == '>') {
				state = 0;
			} else if (isalpha(c)) {
				state = 4;
				grow_reset(&pn);
				grow_put(&pn, tolower(c));
			}
			break;
		case 4:
			if (c == '=') {
				state = 5;
			} else if (isalpha(c)) {
				grow_put(&pn, tolower(c));
			} else {
				state = 3;
			}
			break;
		case 5:
			grow_reset(&pv);
			if (c == '"') {
				state = 6;
			} else if (c == '\'') {
				state = 7;
			} else if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
				state = 3;
			} else {
				grow_put(&pv, c);
				state = 8;
			}
			break;
		case 6:
			if (c == '\"') {
				otag(&tn, &pn, &pv, cb, ctx);
				state = 3;
			} else {
				grow_put(&pv,c);
			}
			break;
		case 7:
			if (c == '\'') {
				otag(&tn, &pn, &pv, cb, ctx);
				state = 3;
			} else {
				grow_put(&pv,c);
			}
			break;
		case 8:
			if (c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '>') {
				otag(&tn, &pn, &pv, cb, ctx);
				if (c == '>') {
					state = 0;
				} else {
					state = 3;
				}
			} else {
				grow_put(&pv, c);
			}
			break;
		}
	}
	grow_free(&tn);
	grow_free(&pn);
	grow_free(&pv);
}

void parse(char *input, parser_cb_t cb, void *ctx)
{
	generic_parse(input, cb, ctx);
}

