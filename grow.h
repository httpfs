#include <stdlib.h>
#include <string.h>

#define INIT_SIZE 128
#define grow_reset(g) grow_set_ptr(g, 0)
#define GROW_INIT { NULL, 0, 0 }

/* TODO: 
 * checkovani nedostatku pameti
 * prejit na size_t a off_t pointery
 */

typedef enum {
	GROW_NORESIZE = 1,
	GROW_STATIC = 2,
} grow_flags;

struct grow {
	char *s;
	int size, ptr;
	grow_flags fl;
};


static int grow_enlarge(struct grow *g, int size)
{
	int ns = g->size;

	if (g->fl & GROW_NORESIZE)
		return 0;

	if (!ns)
		ns = INIT_SIZE;

	while (g->ptr + size > ns)
		ns *= 2;

	if (ns != g->size) {
		g->s = realloc(g->s, ns);
		g->size = ns;
	}
	return 1;
}

static inline int grow_append(struct grow *g, const char *data, int size)
{
	int sz = size;
	if (g->ptr + size > g->size && !grow_enlarge(g, size)) {
		sz = g->size - g->ptr;
	} 

	memcpy(&g->s[g->ptr], data, sz);
	g->ptr += sz;
	return sz;
}

static inline int grow_put(struct grow *g, char c)
{
	if (g->ptr + 1 > g->size) {
		if (!grow_enlarge(g, 1))
			return 0;
	}
	g->s[g->ptr++] = c;
	return 1;
}

static inline void grow_puts(struct grow *g, const char *s)
{
	grow_append(g, s, strlen(s)+1);
	g->ptr--;
}


static inline void grow_init(struct grow *g)
{
	g->s = NULL;
	g->size = 0;
	g->ptr = 0;
	g->fl = 0;
}

static inline void grow_init_static(struct grow *g, char *s, int size)
{
	g->s = s;
	g->ptr = 0;
	g->size = size;
	g->fl = GROW_NORESIZE|GROW_STATIC;
}

static inline void grow_free(struct grow *g)
{
	if (!(g->fl & GROW_STATIC))
		free(g->s);
}

static inline size_t grow_get_ptr(struct grow *g)
{
	return g->ptr;
}

static inline void grow_set_ptr(struct grow *g, size_t ptr)
{
	g->ptr = ptr;
}

static inline char *grow_cstr(struct grow *g)
{
	if (grow_put(g, 0))
		g->ptr--;
	return g->s;
}

