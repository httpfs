#include <stdio.h>
#include <ctype.h>
#include "grow.h"
#include "urlencode.h"



static inline unsigned char nib2hex(int c)
{
	return (c>=10 ? c+'a'-10 : c + '0');
}

static inline unsigned int hex2nib(unsigned char c)
{
	if (isdigit(c)) 
		return c - '0';
	if (c >= 'a' && c <= 'f') 
		return c - 'a' + 10;
	if (c >= 'A' && c <= 'F') 
		return c - 'A' + 10;
	return 0;
}


void path_encode(struct grow *g, const char *path)
{
	unsigned char c;
	char buf[3];

	buf[0] = '%';

	while ((c = *path++)) {
		if (isalnum(c) || c == '_' || c == '/' || c == '.') {
			grow_put(g, c);
		} else {
			buf[1] = nib2hex(c>>4);
			buf[2] = nib2hex(c&15);
			grow_append(g, buf, 3);
		}
	}
}

void path_decode(struct grow *g, const char *path)
{
	unsigned char c, d;

	while ((c = *path++)) {
		if (c == '%') {
			d = hex2nib(*path++) << 4;
			d += hex2nib(*path++);
			grow_put(g, d);
		} else if (c == '&' && !strncasecmp(path, "amp;", 4)) {
			grow_put(g, '&');
			path += 4;
/* 		} else if (c == '+') { */
/* 			grow_put(g, ' '); */
		} else {
			grow_put(g, c);
		}
	}
}

#ifdef EMO

struct grow g = GROW_INIT, h = GROW_INIT;

int main(int argc, char *argv[])
{
	path_encode(&g, argv[1]);
	path_decode(&h, argv[2]);
	grow_put(&g, 0);
	grow_put(&h, 0);

	printf("'%s' '%s'\n", g.arr, h.arr);
}

#endif

#ifdef MAIN
struct grow g = GROW_INIT, h = GROW_INIT;

int main(int argc, char *argv[])
{
	char *p=argv[1], *r=argv[2];
	char *pfx=argv[3];
	if (argc < 4) {
		printf("usage: %s <http_root> <path> <prefix>\n", argv[0]);
		return 1;
	}
	while (*p == *r) {
		p++;
		r++;
	}
	while (*r == '/')
		r++;

	grow_puts(&g, pfx);
	if (pfx[strlen(pfx)-1] != '/')
		grow_put(&g, '/');
	path_encode(&g, r);

	printf("%s\n", grow_cstr(&g));
	return 0;
}

#endif
