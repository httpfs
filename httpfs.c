#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <curl/curl.h>
#include "cache.h"
#include "grow.h"
#include "fetch.h"
#include "parser.h"
#include "urlencode.h"
#include "canonize.h"
#include "debug.h"

#define INDEX_FILE "index.httpfs.html"
#define IN_ORDER_THRESHOLD	(512*1024)
#define OUT_ORDER_THRESHOLD	(1024*1024)
#define SEEK_FORWARD_THRESHOLD  (64*1024)
/*
 * TODO
 * 	konfigurovatelnost
 * 	lepsi parsery (pro apache)
 * 	readahead
 * 	ftp support?
 */

char *base_url;
CURL *conn;
int show_index = 1;
pthread_mutex_t conn_lock = PTHREAD_MUTEX_INITIALIZER;

static inline CURL *get_conn(void)
{
	pthread_mutex_lock(&conn_lock);
	return conn;
}

static inline void put_conn(CURL *conn)
{
	pthread_mutex_unlock(&conn_lock);
}

static inline int is_a_index(char *s)
{
	char *bn;

	if (!show_index) 
		return 0;

	bn = strrchr(s, '/');
	if (bn && !strcmp(bn+1, INDEX_FILE)) {
		*(bn+1) = 0;
		return 1;
	}
	return 0;
}

void mkurl(struct grow *url, const char *path, int slashend)
{
	grow_init(url);
	grow_puts(url, base_url);
	path_encode(url, path);

	if (url->s[url->ptr-1] == '/') {
		url->s[url->ptr-1] = 0;
		url->ptr--;
	}

	if (slashend)
		grow_puts(url, "/");
	else
		grow_put(url, 0);
}

static int chunk_init(struct handle *h)
{
	h->type = HAND_RANDOM;
	grow_init(&h->data);
	return 0;
}

static int chunk_read(struct handle *h, char *buf, size_t size, off_t offset)
{
	CURL *conn;
	int ret;

	grow_init_static(&h->data, buf, size);

	conn = get_conn();
	if (!conn) 
		return -EIO;
	ret = request(conn, h->url.s, 0, NULL, &h->data, size, offset);
	if (ret == -ESPIPE) 
		ret = 0;
	put_conn(conn);
	return ret;
}

static void chunk_fini(struct handle *h)
{
	return;
}


/* actually, this will copy data several times:
 * network stack -> curl buffer
 * curl buffer -> grow buffer
 * grow buffer -> fuse buffer
 * fuse buffer -> kernel buffer
 * kernel buffer -> user buffer
 *
 * that's FIVE TIMES!
 */
#define min(a,b) ((a)<(b)?(a):(b))

static int seq_init(struct handle *h, off_t offset, size_t size)
{
	int ret;

	ret = request_open_async(h, offset, size);
	if (ret < 0) 
		return ret;

	grow_init(&h->lover);
	h->type = HAND_HARD;
	return 0;
}

static void seq_fini(struct handle *h)
{
	if (!request_done(h))
		request_close_async(h);
	grow_free(&h->lover);
}

static int seq_read(struct handle *h, char *buf, size_t size, off_t offset)
{
	int done = 0, ret = 0;
	struct grow *lover = &h->lover;

	if (request_done(h)) {
		if (lover->ptr) 
			goto fill;
		return ret;
	}

	if (lover->ptr < size) {
		ret = request_perf_async(h, size, &done);
		if (done) 
			request_close_async(h);
		/* TODO sem prijde reconnect na timeout */
	} 

	if (ret < 0) 
		return ret;

fill:
	ret = min(lover->ptr, size);
	if (buf) {
		memcpy(buf, lover->s, ret);
	}
	lover->ptr -= ret;
	memmove(lover->s, lover->s+ret, lover->ptr);

	return ret;
}


/* TODO: mergnout to s getattrem (hint: isindex a pak zmenit prava) */
static int determine_size(const char *path, off_t *size)
{
	CURL *conn;
	struct grow url;
	struct stat stb;
	int ret, index;

	memset(&stb, 0, sizeof(struct stat));
	stb.st_size = -1;

	conn = get_conn();
	if (!conn)
		return -EIO;

	mkurl(&url, path, 0);
	index = is_a_index(url.s);
	ret = request(conn, url.s, 1, &stb, NULL, 0, 0);	
	put_conn(conn);
	grow_free(&url);

	*size = stb.st_size;
	return ret;
}


static int httpfs_getdir(const char *path, fuse_cache_dirh_t h, fuse_cache_dirfil_t filler)
{
	struct stat stb;
	struct grow url, page = GROW_INIT;
	struct canon c = { .h = h, .filler = filler };
	int ret = 0;
	CURL *conn;
	
	conn = get_conn();
	if (!conn)
		return -EIO;

	memset(&stb, 0, sizeof(stb));
	stb.st_size = 0;
	stb.st_mode = S_IFDIR | 0555;

	filler(h, ".", &stb);
	filler(h, "..", &stb);

	if (show_index) {
		stb.st_mode = S_IFREG | 0444;
		filler(h, INDEX_FILE, &stb);
	}

	mkurl(&url, path, 1);

	ret = request(conn, url.s, 0, NULL, &page, 0, 0);
	if (ret < 0)
		goto err;

	ret = 0;
	grow_put(&page, 0);
	parse(page.s, canonize, &c);
err:
	grow_free(&url);
	grow_free(&page);
	put_conn(conn);
	return ret;
}


static int httpfs_getattr(const char *path, struct stat *stb)
{
	struct grow url;
	CURL *conn;
	int ret, index;

	memset(stb, 0, sizeof(struct stat));

	if (!strcmp(path, "/")) {
		stb->st_mode = S_IFDIR | 0555;
		stb->st_size = 0;
		return 0;
	}

	conn = get_conn();
	if (!conn)
		return -EIO;

	mkurl(&url, path, 0);
	index = is_a_index(url.s);

	stb->st_mode = S_IFREG | 0444;
	ret = request(conn, url.s, 1, stb, NULL, 0, 0);
	if (ret == -EMLINK) {
		stb->st_mode = S_IFDIR | 0555;
		stb->st_size = 0;
		ret = 0;
	}

	if (index)
		stb->st_mode = S_IFREG | 0555;

	put_conn(conn);
	grow_free(&url);

	return ret;
}

static int httpfs_open(const char *path, struct fuse_file_info *fi)
{
	struct handle *h;
	int ret;
	off_t size;

	dbg("open of '%s' with mode '%x'\n", path, fi->flags);

	if ((fi->flags & 3) != O_RDONLY)
		return -EACCES;

	/* dry run, determine size and/or support of partial content */	
	ret = determine_size(path, &size);
	if (ret < 0)
		return ret;
	dbg("page '%s': %s\n", path, size < 0 ? "no content len" : "content len");


	h = calloc(sizeof(*h), 1);
	mkurl(&h->url, path, 0);
	is_a_index(h->url.s);
	pthread_mutex_init(&h->async_lock, NULL);
	
	if (size >= 0) {
		h->total = size;
		chunk_init(h);
	} else {
		seq_init(h, 0, 0);
	}
/* 	h->type |= HAND_HARD; */

	fi->fh = (unsigned long) h;
	return 0;
}


static int httpfs_read(const char *path, char *buf, size_t size,
		       off_t offset, struct fuse_file_info *fi)
{
	struct handle *h;
	int ret;
	h = (struct handle *) (uintptr_t) fi->fh;

	pthread_mutex_lock(&h->async_lock);
	if (!(h->type & HAND_HARD) && offset < h->total) {
		if ((h->type & HAND_RANDOM) && h->in_order > IN_ORDER_THRESHOLD) {
			dbg("READ: switching to SEQUENTIAL\n");
			chunk_fini(h);
			ret = seq_init(h, offset, h->total - offset);
			if (ret) {
				dbg("READ: cannot switch to sequential (%d), forcing SOFT HARD\n", ret);
				h->type = HAND_RANDOM | HAND_HARD;
			} else {
				h->type = 0;
			}
		} else if (!(h->type & HAND_RANDOM) && h->out_order > OUT_ORDER_THRESHOLD) {
			dbg("READ: switching to RANDOM\n");
			seq_fini(h);
			chunk_init(h);
			h->type = HAND_RANDOM;
		}
	}

	dbg("read: %s %s ho:%lld io:%lld oo:%lld o:%lld s:%llu\n", (h->type&HAND_HARD)?"hard":"soft", (h->type&HAND_RANDOM)?"rand":"seq", h->offset, h->in_order, h->out_order, offset, size);

	if (h->offset == offset) {
		if (h->type & HAND_RANDOM) {
			dbg("READ: reading next chunk RANDOM\n");
			ret = chunk_read(h, buf, size, offset);
		} else {
			dbg("READ: reading next chunk SEQUENTIAL\n");
			ret = seq_read(h, buf, size, offset);
		}
		if (ret >= 0) {
			h->offset += ret;
			h->in_order += ret;
			h->out_order = 0;
		} else {
			h->in_order = 0;
			h->offset = -1;
		}
	} else {
		if ((h->type & HAND_HARD) && !(h->type & HAND_RANDOM)) {
			dbg("READ: reading chunk HARD SEQUENTIAL out of order\n");
			ret = -ESPIPE;
			if (h->offset < offset && offset - h->offset < h->total) {
				dbg("READ: small skip forwards: %d\n", offset - h->offset);
				seq_read(h, NULL, offset - h->offset, h->offset);
				ret = seq_read(h, buf, size, offset);
				if (ret >= 0) {
					h->offset = offset + ret;
				}
			}
		} else if (h->type & HAND_RANDOM) {
			dbg("READ: reading chunk SOFT RANDOM out of order\n");
			ret = chunk_read(h, buf, size, offset);
			if (ret >= 0) {
				h->offset = offset+ret;
				h->in_order = size;
			}
		} else {
			dbg("READ: reading chunk SOFT SEQUENTIAL out of order with chunk\n");
			ret = chunk_read(h, buf, size, offset);
			if (ret >= 0) {
				h->out_order += size;
			}
		}
	}
	pthread_mutex_unlock(&h->async_lock);
	return ret;
}

static int httpfs_release(const char *path, struct fuse_file_info *fi)
{
	struct handle *h;
	h = (struct handle *) (uintptr_t) fi->fh;

	if (h->type & HAND_RANDOM) {
		chunk_fini(h);
	} else {
		seq_fini(h);
	}
	grow_free(&h->url);
	free(h);
	return 0;
}

static int httpfs_opendir(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static int httpfs_releasedir(const char *path, struct fuse_file_info *fi)
{
	return 0;
}



static struct fuse_cache_operations httpfs_oper = {
	.oper = {
		 .getattr = httpfs_getattr,
		 .open = httpfs_open,
		 .release = httpfs_release,
		 .opendir = httpfs_opendir,
		 .releasedir = httpfs_releasedir,
		 .read = httpfs_read,
		 },
	.cache_getdir = httpfs_getdir,
};



static int httpfs_fuse_main(struct fuse_args *args)
{
#if FUSE_VERSION >= 26
	return fuse_main(args->argc, args->argv, cache_init(&httpfs_oper), NULL);
#else
	return fuse_main(args->argc, args->argv, cache_init(&httpfs_oper));
#endif
}


int main(int argc, char *argv[])
{
	struct fuse_args args = FUSE_ARGS_INIT(argc-1, argv+1);
	int res;

	g_thread_init(NULL);

	base_url = argv[1];
	if (base_url[strlen(base_url)-1] == '/')
		base_url[strlen(base_url)-1] = 0;
		
	conn = curl_easy_init();

	res = cache_parse_options(&args);
	if (res == -1)
		exit(1);

	res = httpfs_fuse_main(&args);
	fuse_opt_free_args(&args);

	return res;
}
