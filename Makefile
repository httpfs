CC=gcc
CFLAGS=-D_REENTRANT -DFUSE_USE_VERSION=26 -D_FILE_OFFSET_BITS=64 -I/usr/include/fuse -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -DEBUG
LDFLAGS=-lfuse -lpthread -lglib-2.0 -lgthread-2.0 -lcurl

all: httpfs ncp install urlencode
urlencode: urlencode.c
	$(CC) urlencode.c -o urlencode -g -Wall -DMAIN
ncp: ncp.c
	$(CC) ncp.c -o ncp -g -Wall
httpfs: httpfs.o cache.o urlencode.o fetch.o parser.o canonize.o
canonize.o: canonize.c canonize.h
parser.o: parser.c parser.h
urlencode.o: urlencode.c urlencode.h grow.h
fetch.o: fetch.c fetch.h
cache.o: cache.c cache.h
httpfs.o: httpfs.c

clean:
	rm *.o httpfs ncp
install:
	cp urlencode httpfs ncp ~/b
