typedef void (*parser_cb_t) (void *ctx, char *url, time_t mtime, size_t size);
void parse(char *input, parser_cb_t cb, void *ctx);
